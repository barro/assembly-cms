addImage = (image) ->
    image_root = "http://assembly.galleria.fi#{escape image}"
    $("<div class='item carousel-image'>" +
      "<a href='#{image_root}'>" +
      "<img class='carousel-image' src='#{image_root}?img=smaller' alt='#{escape image}'/>" +
      "</a>" +
      "</div>").appendTo $("#galleriafiCarousel .carousel-inner")

$(document).ready ->
    $.getJSON $("#galleriafiCarousel").data("url"), (data) ->
        images = _.shuffle data
        index = 3
        for image in images[0...index]
            addImage image
        # Make the first image active.
        $("#galleriafiCarousel div.item").first().addClass('active');
        $("#galleriafiCarousel").carousel {interval: 3000}
        $("#galleriafiCarousel").on "slid", ->
            if index < images.length
                addImage images[index]
                index += 1
