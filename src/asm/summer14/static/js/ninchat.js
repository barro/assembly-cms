window.NinchatAsyncInit = function() {
    var languages = {
    fi: {
        language:       'fi',
        audienceQueues: '239dqg97004',
        userName:       'Asiakas',
        titleText:      'Infodesk-chat',
        translations:   {
        'Join audience queue {{audienceQueue.queue_attrs.name}}': 'Juttele Infodeskin kanssa',
        'Join audience queue {{audienceQueue.queue_attrs.name}} (closed)': 'Tilapäisesti suljettu',
        'Joined audience queue {{audienceQueue.queue_attrs.name}}, you are at position {{audienceQueue.queue_position}}.': 'Olette jonossa sijalla {{audienceQueue.queue_position}}.',
        'Audience in queue {{queue}} accepted.': 'Hei, kuinka voimme auttaa?',
        'Joined audience queue {{audienceQueue.queue_attrs.name}}, you are next.': 'Olet seuraavana jonossa.',
        'Enter your message': 'Kirjoita viesti'
            },
        motd:           (
        '<ul class="links unstyled">' +
            '<li><a href="http://www.assembly.org/summer14/manual/faq" target="tausta">Usein kysytyt kysymykset</a></li>' +
            '</ul>' +
                '<p>' +
                'Tästä saat suoran yhteyden avuliaaseen Assemblyn Infodeskiin.' +
                '</p>'
        )
    },

    en: {
        language:       'en',
        audienceQueues: '239dqg97004',
        userName:       'Customer',
        titleText:      'Infodesk chat',
        translations:   {
        'Join audience queue {{audienceQueue.queue_attrs.name}}':'Chat with Infodesk',
        'Join audience queue {{audienceQueue.queue_attrs.name}} (closed)': 'Temporarily closed',
        'Joined audience queue {{audienceQueue.queue_attrs.name}}, you are {{audienceQueue.queue_position}}.': 'You are in queue. Your position: {{audienceQueue.queue_position}}.',
        'Audience in queue {{queue}} accepted.': 'Hello, how can we help you?',
        'Joined audience queue {{audienceQueue.queue_attrs.name}}, you are next.': 'You are next in line.',
        'Enter your message': 'Enter your message'
        },
        motd:           (
        '<ul class="links unstyled">' +
            '<li><a href="http://www.assembly.org/summer14/manual/faq" target="tausta">Frequently asked questions</a></li>' +
            '</ul>' +
                '<p>' +
                'Here you can directly chat with helpful Assembly Infodesk people.' +
                '</p>'
        )
    }
    };

    var lang = document.documentElement.getAttribute('lang') || 'en';

    var language = languages[lang] || languages.en;

    window.Ninchat.embedInit({
    audienceRealmId: '239dq423004',
    audienceQueues:  language.audienceQueues,
    userName:        language.userName,
    remember:        true,
    audienceRating:  true,
    language:        language.language,
    titleBackground: 'black',
    titleText:       language.titleText,
    translations:    language.translations,
    motd:            (
        '<style>' +
        '.motd time { display: none; }' +
        '.motd ul { border-top: 1px solid #ccc; }' +
        '.motd li { border-bottom: 1px solid #ccc; padding: 5px; text-align:     left; }' +
        '.motd li:hover { cursor: pointer; border-bottom: 1px solid #ccc; padding: 5px; }' +
        '.motd li a { display: block; text-align: center; text-decoration: none !important;  }' +
        '.motd p { font-size: .9em; text-align: center; }' +
            '</style>' +
            language.motd
        ),
        audienceMetadata: {
            URL: window.location.href,
            Referrer: document.referrer,
        }
    });
};

(function(doc) {
    if (doc.getElementById('ninchat-js')) {
        return;
    }

    var js, first = doc.getElementsByTagName('script')[0];

    js     = doc.createElement('script');
    js.id  = 'ninchat-js';
    js.src = 'https://ninchat.com/js/embed.min.js?_' + new Date().getDate();
    first.parentNode.insertBefore(js, first);
}(document));
