$(function() {
    var KEY_ESCAPE = 27;
    var STREAM_IMAGE_BASE = "@@/asm.winter15/images/stream-buttons/";
    var STREAM_OFFLINE = "offline";
    var STREAM_ONLINE = "online";

    function setStreamLinkImage(link_element, type) {
        var link_image = link_element.data("image-" + type);
        link_element.css(
            "background-image",
            "url(" +
                STREAM_IMAGE_BASE + link_image + ")");
    }

    function isEventOngoing(now, event) {
        if (new Date(event.start_time) < now && now < new Date(event.end_time)){
            return true;
        }
        return false;
    }

    function updateStreamOnlineStatus(link_elements, events) {
        if (!events) {
            return;
        }

        var now = new Date();
        $.each(link_elements, function(n, link_element){
            setStreamLinkImage(link_element, STREAM_OFFLINE);
            $.each(events, function(num, event) {
                var link_flag = link_element.data("flag");
                if ($.inArray(link_flag, event.flags) >= 0 &&
                    isEventOngoing(now, event)) {
                    setStreamLinkImage(link_element, STREAM_ONLINE);
                    // break loop
                    return false;
                }
            });
        });
    }

    function showPlayerAsmtv()
    {
        $("#streamplayer").html(
            '<iframe src="http://www.assemblytv.net/player?host=stream2&file=tvsd.stream" width="530" height="340" border="0" style="border:0;">' +
            '</iframe>' +
            '<div class="stream-description">' +
            '<p>' +
            'SD (720x576) RTSP stream for ' +
                '<a href="http://www.videolan.org/vlc/">VLC</a> ' +
            '(and other standalone players/or equivalent): ' +
            '<a href="rtsp://stream2.asm.fi:1935/live/tvsd.stream">' +
            'rtsp://stream2.asm.fi:1935/live/tvsd.stream' +
            '</a>' +
            '</p><p>' +
            'HD (1920x1080) RTSP stream for ' +
                '<a href="http://www.videolan.org/vlc/">VLC</a> ' +
            '(and other standalone players/or equivalent): ' +
            '<a href="rtsp://stream2.asm.fi:1935/live/tvhd.stream">' +
            'rtsp://stream2.asm.fi:1935/live/tvhd.stream' +
            '</a>' +
            '</p>' +
            '</div>'
);
        $("#streamplayer").css('width','530px');
        $("#streamplayer").css('margin-left','20px');
    }

    function showPlayerArttech()
    {
        $("#streamplayer").html(
            '<iframe src="http://www.assemblytv.net/player?host=stream2&file=sem.stream" width="530" height="340" border="0" style="border:0;">' +
            '</iframe>' +
            '<div class="stream-description">' +
            '<p>' +
            'Seminar RTSP stream (1280x720) for ' +
            '<a href="http://www.videolan.org/vlc/">VLC</a> ' +
            '(and other standalone players/or equivalent): ' +
            '<a href="rtsp://stream2.asm.fi:1935/live/sem.stream">' +
                'rtsp://stream2.asm.fi:1935/live/sem.stream' +
            '</a>' +
            '</p>' +
            '</div>'
        );
        $("#streamplayer").css('width','530px');
        $("#streamplayer").css('margin-left','30px');
    }

    function showPlayerTwitch(channel, description)
    {
        $("#streamplayer").html(
            '<object type="application/x-shockwave-flash" height="396" width="650" id="live_embed_player_flash" data="http://www.twitch.tv/widgets/live_embed_player.swf?channel="' +
                channel +
            '" bgcolor="#000000">' +
            '<param name="allowFullScreen" value="true" />' +
            '<param name="allowScriptAccess" value="always" />' +
            '<param name="allowNetworking" value="all" />' +
            '<param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" />' +
            '<param name="flashvars" value="hostname=www.twitch.tv&channel=' +
                channel +
            '&auto_play=true&start_volume=25" />' +
            '</object>' +
            '<a href="http://www.twitch.tv/' +
                channel +
             '" class="trk" style="padding:2px 0px 4px; display:block; font-weight:normal; font-size:10px; text-decoration:underline; text-align:center;">' +
            description +
            '</a>'
        );
        $("#streamplayer").css('width','650px');
        $("#streamplayer").css('margin-left','-50px');
    }

    function showPlayerStarcraft2()
    {
        showPlayerTwitch("suprDivinesia", "Watch live video from Starcraft II on www.twitch.tv");
    }

    function showPlayerDota2()
    {
        showPlayerTwitch("assemblymoba", "Watch live video from Dota 2 on www.twitch.tv");
    }

    function showPlayerCsgo()
    {
        showPlayerTwitch("roomonfire", "Watch live video from CS:GO on www.twitch.tv");
    }

    function showPlayerHearthstone()
    {
        showPlayerTwitch("assemblyhs", "Watch live video from Hearthstone on www.twitch.tv");
    }

    function showPlayerStreetfighter()
    {
        showPlayerTwitch("ptprautanyrkki", "Watch live video from Ultra Street Fighter IV on www.twitch.tv");
    }

    function showPlayerTekken()
    {
        showPlayerTwitch("ptprautanyrkki", "Watch live video from Tekken Tag Tournament 2 on www.twitch.tv");
    }

    function removeHash () {
        history.pushState(
            "",
            document.title,
            window.location.pathname + window.location.search
        );
    }

    function hidePlayer()
    {
        $("#streamplayer").remove();
        $("#overlay").remove();
        removeHash();
    }

    function showStream(event) {
        var link_element = event.data;
        var stream = link_element.data("flag");

        var stream_players = {
            // assytv: showPlayerAsmtv,
            // seminar: showPlayerArttech,
            // scii: showPlayerStarcraft2,
            // dota: showPlayerDota2,
            // csgo: showPlayerCsgo,
            // hs: showPlayerHearthstone,
            // ssf: showPlayerStreetfighter,
            // tekken: showPlayerTekken
        };

        var stream_player = stream_players[stream];
        if (!stream_player) {
            console.log("Stream not found: " + stream);
            $("#streamplayer").remove();
            return;
        }

        // Do not react to a link click
        event.preventDefault();

        var overlay = jQuery('<div id="overlay"></div>');
        overlay.appendTo(document.body);

        overlay.click(function(e) {
            hidePlayer();
        });
        $(document).keydown(function(e) {
            if (e.which == KEY_ESCAPE) {
                hidePlayer();
            }
        });

        var videostream = jQuery('<div id="streamplayer" style="display: none;"></div>');
        videostream.insertAfter(jQuery(".streamlinks"));
        $("#streamplayer").show();

        document.location.hash = "#show-stream-" + stream;
        stream_player();
    }

    function onload() {
        var events = null;
        var stream_links = jQuery(".streamlink");
        var link_elements = [];
        stream_links.each(function(index, link) {
            var link_element = jQuery(link);
            link_elements.push(link_element);
            setStreamLinkImage(link_element, STREAM_OFFLINE);
            link_element.click(link_element, showStream);
        });

        function updateEventsData() {
            $.getJSON("schedule/json", function(data) {
                events = data.events;
                updateStreamOnlineStatus(link_elements, events);
            });
        }

        updateEventsData();
        window.setInterval(updateEventsData, 179999);

        window.setInterval(function() {
            updateStreamOnlineStatus(link_elements, events);
        }, 9001);
    }

    onload();
});



