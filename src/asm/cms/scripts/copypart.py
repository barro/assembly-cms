# Copyright (c) 2010-2011 Assembly Organizing
# See also LICENSE.txt

import argparse
import asm.cms.cms
import sys
import transaction
import zope.app.component.hooks
import zope.copypastemove.interfaces


def get_new_page(new_root, old_page):
    # Construct path of the old page
    path = []
    needle = old_page
    while not isinstance(needle, asm.cms.cms.CMS):
        path.insert(0, needle.__name__)
        needle = needle.__parent__
    # Traverse path in the new root and construct pages that don't exist along
    # the way.
    new_page = new_root
    p = path[:]
    while p:
        name = p.pop(0)
        if name not in new_page:
            new_page[name] = asm.cms.page.Page(old_page.type)
        new_page = new_page[name]
    new_page.type = old_page.type
    return new_page, path

def traverse(root, path):
	path = path.split('/')
	obj = root
	while path:
		next = path.pop(0)
		if not next:
			continue
		obj = obj[next]
	return obj


parser = argparse.ArgumentParser(
    description='Copy parts of a site to another place.')

parser.add_argument('old', help='the path of the existing content')
parser.add_argument('new', help='the path of the new content (has to exist already)')

args = parser.parse_args()

source = traverse(root, args.old)
new = traverse(root, args.new)

transaction.savepoint()

site = root[args.new.split('/')[1]]
zope.app.component.hooks.setSite(site)

old_pages = [source]
while old_pages:
    old_page = old_pages.pop(0)
    new_page, path = get_new_page(new, old_page)
    print 'Copying', '/' + '/'.join(path)
    for edition in list(new_page.editions):
        del new_page[edition.__name__]
    for old_edition in old_page.editions:
        new_edition = new_page.addEdition(old_edition.parameters)
        new_edition.copyFrom(old_edition)
    old_pages.extend(old_page.subpages)

transaction.commit()
