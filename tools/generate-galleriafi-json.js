#!/usr/bin/env node

var folder = 'Assembly+Summer+2013/'
	, outFile = 'images.json';

var request, async
	, fs = require('fs')
	, baseURL = 'http://assembly.galleria.fi/'
	, folderJSON = '?type=getFolderTree&folder='
	, imageJSON = '?type=getFileListJSON&id=';

try {
	request = require('request');
	async = require('async');
} catch (e) {
	console.log('Missing dependencies run the following command and try again.');
	console.log('npm install request async');
	process.exit();
}

// Here's where all the magic happens.
async.waterfall([getFolders, joinPhotos, saveJSON], allDone);

function getFolders(cb) {
	process.stdout.write('Generating JSON for ' + folder);
	request(baseURL + folderJSON + folder, function (err, res, body) {
		var data = JSON.parse(body);
		// Save a list of folder ids and paths.
		cb(err, Object.keys(data).map(function (f) { return {id: data[f].id, path: f}; }));
	});
}

function joinPhotos(folders, cb) {
	console.log(' found', folders.length, 'folders.');
	async.concatSeries(folders, getPhotos, cb);
}

function getPhotos(folder, cb) {
	process.stdout.write('Getting photos from ' + folder.path);
	request(baseURL + imageJSON + folder.id, function (err, res, body) {
		if (err) { console.log('Failed to get', folder.path, err.stack); return cb(err); };
		var data = JSON.parse(body)
			, photos = data.map(function (i) { return i.filepath; });

		console.log(' found ' + photos.length + ' photos');
		cb(null, photos);
	});
}

function saveJSON(photos, cb) {
	process.stdout.write('Found ' + photos.length + ' photos in total.');
	fs.writeFile(outFile, JSON.stringify(photos), cb);
}

function allDone(err) {
	if (err) { return console.log(' -> There was an error\n', err.stack); }
	console.log(' -> Successfully wrote json to', outFile);
}